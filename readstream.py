#!/usr/bin/env python
# -*- coding: utf-8 -*-

import schedule
from Queue import Queue
import urllib, json

titleBus = Queue()
titleBus.put("JudasKiss - screaming for your love")

def readStream():
	currentTitle = titleBus.get()
	
	url = "https://rc1.nobexinc.com//nowplaying.ashx?stationid=70642"
	response = urllib.urlopen(url)
	data = json.loads(response.read())
		
	title = data['artist'] + ' - ' + data['songName']
	
	if (title != currentTitle):
		print "New title : " + title
		titleBus.put(title)
	else:
		titleBus.put(currentTitle)

schedule.every(5).seconds.do(readStream)

while True:
	schedule.run_pending()
