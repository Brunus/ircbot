#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Simple IRC bot able to parse a json stream privided by an online radioperfecto
This code is under MIT licence
Code by Brunus
Diaspora account : brunus@framasphere.org
Mastodon account : brunus@framapiaf.org
'''

import socket
import ssl
import time
import json
import urllib
import threading
from Queue import Queue
import os

nickBus = Queue()
chanelBus = Queue()
tagBus = Queue()
jobBus = Queue()
bandBus = Queue()
blindBus = Queue()
gameBus = Queue()

band = ''
tagBus.put("JudasKiss - screaming for your love")
bandBus.put ("Mötley Crap")

gamers = dict()

# The bot's administrators list, with levels.
# Level 0 is the highest level, more admin value is high, less is the level
admins = dict({'brunus':0 ,"brunus_MIPS":0})

killThreads = False
blindMode = False
blindBus.put(blindMode)

## Settings
### IRC
server = "chat.freenode.net"
port = 6697
# channel = "#radioperfecto"
channel = "#radiobotperfecto"
botnick = "JudassKiss"
# password = "YOURPASSWORD"

irc_C = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #defines the socket
irc = ssl.wrap_socket(irc_C)

print "Establishing connection to [%s]" % (server)
# Connect
irc.connect((server, port))
irc.setblocking(False)
# irc.send("PASS %s\n" % (password))
irc.send("USER "+ botnick +" "+ botnick +" "+ botnick +" :meLon-Test\n")
irc.send("NICK "+ botnick +"\n")
# irc.send("PRIVMSG nickserv :identify %s %s\r\n" % (botnick, password))
irc.send("JOIN "+ channel +"\n")

def ircConnect():
	
	while True:
		
		try:
			chan = irc.recv(2040)
			
			# On Freenode IRC network we know the connexion is done when we receive...
			# This test needs to be setted for the network you want to connect
			if ':End of /NAMES list.' in chan:
				return()
		
		# connecting an IRC server needs time
		except Exception:
			continue

def loadAdmins():
	if os.path.isfile('admins.json'):
		with open('admins.json', 'r') as jsonFile:
			admins = json.load(jsonFile)

def run_threaded(job_func):
	job_thread = threading.Thread(target=job_func)
	jobBus.put(job_thread)
	job_thread.start()

def chanStream():
	
	chan = ''
	
	while not killThreads:
		
		time.sleep(1)
		
		try:
			chan = irc.recv(2040)
			if chan != '':
				chanelBus.put(chan)
					
		except Exception:
			continue

def jsonStream():
	
	url = "https://rc1.nobexinc.com//nowplaying.ashx?stationid=70642"
	
	while not killThreads:
		time.sleep(5)
		
		currentTitle = tagBus.get()
		currentBand = bandBus.get()
		blindMode = blindBus.get()
		blindBus.put(blindMode)
		
		response = urllib.urlopen(url)
		
		try:
			data = json.loads(response.read())
			
			title = data['artist'] + ' - ' + data['songName']
			
			if (title != currentTitle):
				msg = " :New tag : " + title
				if not blindMode:
					irc.send('PRIVMSG '+channel+msg+'\r\n')
				if gameBus.qsize() != 0:
					gameBus.get()
				tagBus.put(title)
				bandBus.put(data['artist'])
				
				# If artist is in users lists, notify users
				notices = json.load(open('notices.json'))
				nickTargets = ''
				for key in notices:
					for values in notices[key]:
						if data['artist'] in values:
							nickBus.put(key)
				if nickBus.qsize() != 0:
					for i in range(nickBus.qsize()):
						nickTargets = nickTargets + nickBus.get() + ', '
					msg = " :" + nickTargets + ', du bon son : ' + data['artist'] + ' !'
					
					if not blindMode:
						irc.send('PRIVMSG '+channel+msg+'\r\n')
				
			else:
				tagBus.put(currentTitle)
				bandBus.put(currentBand)
		except:
			msg = " :Stream problem, stay tuned.. "
			irc.send('PRIVMSG '+channel+msg+'\r\n')
			tagBus.put(currentTitle)
			bandBus.put(currentBand)

def isAdmin(chan, admins):
	t = chan.split('!~')
	nick = t[0].strip()
	nick = nick[1:]
	if nick in admins:
		return True		
	else:
		return False

def addAdmin(chan, admins):
	t = chan.split(':!admin add ')
	if t[0] in admins:
		newAdmin = t[1].split(' ')
		admins.update({newAdmin[0]:int(newAdmin[1])})
		with open('admins.json', 'w') as jsonFile:
			json.dump('admins', jsonFile)

def delAdmin(chan, admins):
	t = chan.split(':!admin del ')
	if t[0] in admins:
		target = t[1].split(' ')
		admins.pop(target)
		with open('admins.json', 'w') as jsonFile:
			json.dump('admins', jsonFile)
		return(target)

def addBand(nick, band):
	
	notices = json.load(open('notices.json'))
	
	if not nick in notices:
		notices.update({nick:[band]})
		with open('notices.json', 'w') as jsonFile:
			json.dump(notices, jsonFile)
	
	else:
		if not band in notices[nick]:
			notices[nick].append(band)
			with open('notices.json', 'w') as jsonFile:
				json.dump(notices, jsonFile)
				
	msg = " :OK " + nick + ", adding " + band + " to notify list" 
	irc.send('PRIVMSG '+channel+msg+'\r\n')

def delBand(nick, band):
	
	notices = json.load(open('notices.json'))

	if not nick in notices:
		msg = " :" + nick + ", you never added any ban. Nothing to do."
		irc.send('PRIVMSG '+channel+msg+'\r\n')
		
	else:
		if band in notices[nick]:
			notices[nick].remove(band)
			with open('notices.json', 'w') as jsonFile:
				json.dump(notices, jsonFile)
			msg = " :OK " + nick + ", deleting " + band + " from notify list" 
			irc.send('PRIVMSG '+channel+msg+'\r\n')
		
		else:
			msg = " :" + nick + ", you did not add this band in your notify list. Nothing to do"
			irc.send('PRIVMSG '+channel+msg+'\r\n')


# First we need to connect the IRC network and to send the bot in the chanel
ircConnect()

# Then we can thread the two parallel processes
# -> chanStream is reading what occurs on the channel and put it un the channel chan Queue
# -> jsonStream is reading the json stream to extract tags ant put in in the tag Queue
run_threaded(chanStream)
run_threaded(jsonStream)

while True:
	
	time.sleep(1)
	
	if chanelBus.qsize() != 0 :
		
		# To identify commands we need to recover chan's events in the chan Queue
		chan = chanelBus.get()
		
		# Quick and dirty code for blind test mode.
		if blindMode:
			if bandBus.qsize() != 0:
				band = bandBus.get()
				bandBus.put(band)
				if band in chan:
					if gameBus.qsize() != 0:
						msg = ' :Wait for next song !'
					else :
						gameBus.put(1)
						nick = chan.split('!')[0]
						msg = ' :Well done ' + nick + ' !'
						if nick in gamers:
							gamers[nick] += 1
						else:
							gamers.update({nick:1})
					
					irc.send('PRIVMSG '+channel+msg+'\r\n')
		
		# commands
		
		## !stop "botNick" : kills the bot
		## Admins only can execute this command
		
		if '!stop' in chan and isAdmin(chan, admins):
				killThreads = True
				while jobBus.qsize() != 0:
					job = jobBus.get()
					print 'Killing Job : ' + job.getName()
					job.join(0)
				exit(0)
		
		if '!admin add' in chan and isAdmin(chan, admins):
			admins.update(addAdmin(chan, admins))
		if '!admin del' in chan and isAdmin(chan, admins):
			admins.pop(delAdmin(chan, admins))
		
		## PONG answer to servers PING : needed to keep the bot alive
		
		if 'PING' in chan:
			irc.send('PONG ' + chan.split() [1] + '\r\n')
		
		## !hi command : simple Hi answer to Hi from users
			
		if ':!hi' in chan:
			t = chan.split(':!hi')
			to = t[1].strip()
			msg = ' :Hello '+str(to)+'!'
			irc.send('PRIVMSG '+channel+msg+'\r\n')

		## !blind start : starting blind test mode
		
		if ':!blind start' in chan and isAdmin(chan, admins):
			for i in range(len(gamers)):
				gamers.popitem()
				
			blindMode = True
			blindBus.get()
			blindBus.put(True)
			msg = ' :Starting blind test !'
			irc.send('PRIVMSG '+channel+msg+'\r\n')
		
		## !blind start : starting blind test mode
		
		if ':!blind stop' in chan and isAdmin(chan, admins):
			blindMode = False
			blindBus.get()
			blindBus.put(False)
			msg = ' :Stopping blind test'
			irc.send('PRIVMSG '+channel+msg+'\r\n')
		
		if ':!blind score' in chan and len(gamers) > 0:
			for key in gamers:
				msg = ' :' + key + ' : ' + str(gamers[key])
				irc.send('PRIVMSG '+channel+msg+'\r\n')
		
		## !add "band name" function : add band name in user's list
		
		if ':!band add' in chan:
			if not blindMode :
				nick = chan.split('!')[0]
				nick = nick[1:]
				band = chan.split(':!band add ')[1]
				band = band[:-2]
				addBand(nick, band)
			else :
				msg = " :Sorry, I'm running a blind test game, I can't add right now."
				irc.send('PRIVMSG '+channel+msg+'\r\n')
		
		## !del "band name" function : del band name from user's list
		
		if ':!band del' in chan:
			nick = chan.split('!')[0]
			nick = nick[1:]
			band = chan.split(':!band del ')[1]
			band = band[:-2]
			delBand(nick, band)
		
		## !add : add the current band in user's list
		
		if ':!add' in chan:
			if bandBus.qsize() != 0:
				band = bandBus.get()
				bandBus.put(band)
			nick = chan.split('!')[0]
			nick = nick[1:]
			addBand(nick, band)
		
		## !del : del the current band from user's list
		
		if ':!del' in chan:
			if bandBus.qsize() != 0:
				band = bandBus.get()
				bandBus.put(band)
			nick = chan.split('!')[0]
			nick = nick[1:]
			delBand(nick, band)
		
