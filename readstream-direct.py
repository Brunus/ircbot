#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib2
import schedule
from Queue import Queue
import re

bus = Queue()
last = Queue()

stream_url = 'http://radioperfecto.net-radio.fr/perfecto.mp3'

# json stream
# https://rc1.nobexinc.com//nowplaying.ashx?stationid=70642

request = urllib2.Request(stream_url)

bus.put("JudasKiss - screaming for your love")
last.put("If you want blood, you'v got it !")

def readStream():
	currentTitle = bus.get()
	lastTitle = last.get()
	try:
		request.add_header('Icy-MetaData', 1)
		response = urllib2.urlopen(request)
		icy_metaint_header = response.headers.get('icy-metaint')
		if icy_metaint_header is not None:
			metaint = int(icy_metaint_header)
			read_buffer = metaint+255
			content = response.read(read_buffer)
			result = re.search("StreamTitle=\'(.*)\';", content[metaint:])
			title = result.group(1)
			if (title != currentTitle):
				if (title == lastTitle):
					print "Error in stream's tags : " + title
					bus.put(currentTitle)
					last.put(lastTitle)
				else:
					print "New title : " + title
					bus.put(title)
					last.put(currentTitle)
			else:
				bus.put(currentTitle)
				last.put(lastTitle)
	except:
		print "Error"
		bus.put(currentTitle)
		last.put(lastTitle)

schedule.every(5).seconds.do(readStream)

while True:
	schedule.run_pending()
