# Online radio dedicated irc bot

This bot is a very simple and quick made IRC bot. It's dedicated to online radio srtream read and blind text games.
The code you will want to test is ircnot.py. There is still hardcoded this, like the json stream url. but you know...release early, release often ! And...KISS !

Python IRC bot doing some stuffs:
- Connect an IRC chanel
- Read an online radio's Json stream
- Send current artist/band name and songs titles on the IRC chanel
- React at some recommandations
- Notify IRC users when current artist/band is playing (based on a notify list)
- Manage an artist/band list for each user (work in progress)
- Delete users lists on demand or if user not connected since long time (to do)
- Manage a blind test game (quick and dirty but working)

Commands are :
- !hi -> The bot will answer "Hi nickname"
- !blind start and !blind stop -> The bot will start blind test game
- !blind score -> The bot will display the actual score (for the current game or the game that just been stopped)
- !add -> The bot will add the current band in a notify list
- !band add bandname -> The bot will add bandname in notify list
- !band del bandnama -> The bot will del bandname from notify list
- !admin add nickname -> The bot will add nickname to the admins list
- !admin del nickname -> The bot will remove nickname from the notify list
- !stop botname -> Will disconnect the bot
